import Vue from 'vue';
import Router from 'vue-router';
import AddStoryList from '@/components/AddStoryList';
import SessionView from '@/components/SessionView';
import Login from '@/components/Login';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/create-session',
      name: 'AddStoryList',
      component: AddStoryList,
    },
    {
      path: '/poker-planning-view/:id',
      name: 'SessionView',
      component: SessionView,
    },
    {
      path: '*',
      redirect: '/login',
    },
  ],
});
