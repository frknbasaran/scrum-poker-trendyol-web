const apiAddr = 'http://api.muuvio.com';
const socketAddr = 'http://api.muuvio.com:1111';
export default {
  apiAddr,
  socketAddr,
};
