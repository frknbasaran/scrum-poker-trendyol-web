// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Resource from 'vue-resource';
import Notification from 'vue-notification';
import Clipboard from 'vue-clipboard2';

import IO from 'vue-socket.io';

/* eslint-disable-next-line */
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './App';
import router from './router';
import constants from './constants';

Clipboard.config.autoSetContainer = true;
Vue.config.productionTip = false;

Vue.use(IO, constants.socketAddr);
Vue.use(BootstrapVue);
Vue.use(Resource);
Vue.use(Notification);
Vue.use(Clipboard);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
